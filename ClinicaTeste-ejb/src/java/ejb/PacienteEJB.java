package ejb;


import entidade.Paciente;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;


@Stateless
@LocalBean
public class PacienteEJB {

    @PersistenceContext(unitName = "ClinicaTeste-ejbPU2")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void alterar(Paciente paciente) {
	em.merge(paciente);
    }
 
    public Paciente consultar(Paciente paciente) {
	return em.find(Paciente.class, paciente.getCpf());
    }
 
    public void excluir(Paciente paciente) {
         Paciente paciente2 = em.find(Paciente.class, paciente.getCpf());
         
           System.out.print(paciente2.toString());
         
        //Call remove method to remove the entity
        if(paciente2 != null){
            em.remove(paciente2);
        }
        
	
    }
 
    public boolean existe(Paciente paciente) {
	return (consultar(paciente) != null);
    }
 
    public void inserir(Paciente paciente) {
	em.persist(paciente);
    }
 
    
    
   
}
