package ejb;

import entidade.Consulta;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Camila da Luz
 */
@Stateless
@LocalBean
public class ConsultaEJB {

    @PersistenceContext(unitName = "ClinicaTeste-ejbPU2")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    public void alterar(Consulta consulta) {
        em.merge(consulta);
    }
 
    public Consulta consultar(Consulta consulta) {
	return em.find(Consulta.class, consulta.getId());
    }
 
    public void excluir(Consulta consulta) {
	consulta = consultar(consulta);
        System.out.print(consulta.toString());
	if (consulta != null) {
            em.remove(consulta);
	}
    }
 
    public boolean existe(Consulta consulta) {
	return (consultar(consulta) != null);
    }
 
    public void inserir(Consulta consulta) {
	em.persist(consulta);
    }
 
    
}
