package ejb;

import entidade.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;


@Stateless
@LocalBean
public class UsuarioEJB {

    @PersistenceContext(unitName = "ClinicaTeste-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    public void alterar(Usuario usuario) {
	em.merge(usuario);
    }
 
    public Usuario consultar(Usuario usuario) {
	return em.find(Usuario.class, usuario.getNickname());
    }
 
    public void excluir(Usuario usuario) {
	usuario = consultar(usuario);
	if (usuario != null) {
            em.remove(usuario);
	}
    }
 
    public boolean existe(Usuario usuario) {
	return (consultar(usuario) != null);
    }
 
    public void inserir(Usuario usuario) {
        em.persist(usuario);
    }
 
	
}
