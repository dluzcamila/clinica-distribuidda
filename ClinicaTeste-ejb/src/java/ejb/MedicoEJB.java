package ejb;

import entidade.Medico;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;


@Stateless
@LocalBean
public class MedicoEJB {

    @PersistenceContext(unitName = "ClinicaTeste-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void alterar(Medico medico) {
	em.merge(medico);
    }
 
    public Medico consultar(Medico medico) {
	return em.find(Medico.class, medico.getCrm());
    }
 
    public void excluir(Medico medico) {
         Medico medico2 = em.find(Medico.class, medico.getCrm());
         
           System.out.print(medico2.toString());
         
        //Call remove method to remove the entity
        if(medico2 != null){
            em.remove(medico2);
        }
        
	
    }
 
    public boolean existe(Medico medico) {
	return (consultar(medico) != null);
    }
 
    public void inserir(Medico medico) {
	em.persist(medico);
    }
 
    
}
