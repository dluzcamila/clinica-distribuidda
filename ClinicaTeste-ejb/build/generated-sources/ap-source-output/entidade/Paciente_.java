package entidade;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-11-23T17:12:05")
@StaticMetamodel(Paciente.class)
public class Paciente_ { 

    public static volatile SingularAttribute<Paciente, String> uf;
    public static volatile SingularAttribute<Paciente, String> telefone;
    public static volatile SingularAttribute<Paciente, String> observacao;
    public static volatile SingularAttribute<Paciente, String> cidade;
    public static volatile SingularAttribute<Paciente, String> rg;
    public static volatile SingularAttribute<Paciente, String> bairro;
    public static volatile SingularAttribute<Paciente, String> cpf;
    public static volatile SingularAttribute<Paciente, String> nome;
    public static volatile SingularAttribute<Paciente, String> dataNascimento;
    public static volatile SingularAttribute<Paciente, String> email;
    public static volatile SingularAttribute<Paciente, String> cep;
    public static volatile SingularAttribute<Paciente, String> rua;

}