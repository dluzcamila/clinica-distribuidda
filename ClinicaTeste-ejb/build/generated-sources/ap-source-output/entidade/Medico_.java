package entidade;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-11-23T17:12:05")
@StaticMetamodel(Medico.class)
public class Medico_ { 

    public static volatile SingularAttribute<Medico, String> telefone;
    public static volatile SingularAttribute<Medico, String> especialidade;
    public static volatile SingularAttribute<Medico, String> rg;
    public static volatile SingularAttribute<Medico, String> cpf;
    public static volatile SingularAttribute<Medico, String> nome;
    public static volatile SingularAttribute<Medico, String> dataNascimento;
    public static volatile SingularAttribute<Medico, String> email;
    public static volatile SingularAttribute<Medico, String> crm;

}