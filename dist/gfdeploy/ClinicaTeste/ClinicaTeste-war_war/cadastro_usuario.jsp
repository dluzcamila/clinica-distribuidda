<%-- 
    Document   : cadasrtoUsuario
    Created on : 09/11/2016, 19:17:19
    Author     : Camila da Luz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Usuário - Cadastro </title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <script>
    window.addEventListener("load",function(){
         alert("${loginError}");
    }
    </script>
    <div id="login-titulo">
	<h1 style = "color:#3CB371; text-align:center; padding-bottom:10px; padding-right:15px;">Clínica Distribuída</h1>
    </div>
    <div id="login-form">
        <div id="login-container">
            <p style="display: block; text-align: center;">Cadastrar Usuário</p>
            <form method="post" action="UsuarioServlet" id="formCadastro" name="formulario" onsubmit="return validarSenha(this);">
                <div class="field"><input name="nome" required placeholder="Nome" value="${usuario.nome}" /></div>
                <div class="field"><input name="nickname" required autofocus="autofocus" placeholder="NickName"  value="${usuario.nickname}"/></div>
                <div class="field"><input name="funcao" required placeholder="Função" value="${usuario.funcao}" /></div>
                <div class="field"><input type="password" required id="txtSenha"  name="senha" placeholder="Senha"  value="${usuario.senha}"/></div>
                <div class="field"><input type="password" required id="repetir_senha" name="repetir_senha" placeholder="Confirmar Senha"></div>
                <div class = "invis">
                    <select name="acao" required>
                        <option selected value="Incluir">Incluir</option>
                        <option value="Alterar">Alterar</option>
                        <option value="Excluir">Excluir</option>
                        <option value="Consultar">Consultar</option>
                    </select>
                </div>
                <div id="btn-login"><input  type="submit" value="Cadastrar" /></div>
                <input type="reset" value="Limpar"> <br />
            </form>
            <a href="login.jsp" style = "text-align:center; margin:0px auto; display:block; color:#008B45;">Login</a>
            <h6 style="display: block; text-align: center;">Clínica Distribuída &copy; 2016</h6>
			
        </div>
    </div>
</body>
</html>
<script>
function validarSenha(form){ 
	senha = document.formulario.senha.value;
	senhaRepetida = document.formulario.repetir_senha.value;
	if (senha != senhaRepetida){
		alert("Repita a senha corretamente");
		document.formulario.repetir_senha.focus();	
		return false;
	}
}
</script>