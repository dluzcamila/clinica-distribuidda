<%@page import="teste.ConectPostgree"%>
<!DOCTYPE html>
<%@page import="teste.conectbanco"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.swing.JComboBox"%>
<html>
<head>
        <title>Consulta - Editar</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="script.js" type="text/javascript"></script>
</head>
<body onload="ready()" onresize="adjust()">
	<nav id="menu">
        <ul>
            <li><a href="consulta.jsp">Consultas</a></li>
            <li><a href="medico.jsp">M�dicos</a></li>
            <li><a href="paciente.jsp">Pacientes</a></li>
        </ul>
    </nav>
    <div id="topbar">
        <h1>Cl�nica Distrubu�da</h1>
        <div id="user_nav">
            <a href="index.html">Sair</a>
        </div>
    </div>
	<div id="sidebar">
            <a href="edit_consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>Editar</a>
            <a href="exclui_consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>Excluir</a>
            <a href="consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>P�gina Inicial</a>
        </div>

	<div id="login-form">
        <div id="login-container2">
            <p style="display: block; width: 500px; margin-left: 150px; padding-bottom: 10px; color:#3CB371; font-size: 20px;">Editar Consulta</p>
            <form method="post" action="ConsultaServlet" id="formCadastro" name="formulario" onsubmit="return validarSenha(this);">
                <select style="width: 460px; height: 33px; margin-bottom: 3px;" name="consulta" class="campos" style="width: 381px;" onkeypress="cbboxSearch(this, event); return false;">
                    <option value="0">Selecione a Consulta</option>
                    <%
                            JComboBox nat_;
                            nat_ = new JComboBox();
                            try{
                                 Connection conn = null;
                                 ConectPostgree bd = new ConectPostgree();
                                 conn = bd.conecta();
                                 String nome_paciente = "";
                                 String nome_medico= "";
                                 PreparedStatement sql = conn.prepareStatement("SELECT id, crm, cpf, horario, dataconsulta FROM consulta");
                                 ResultSet res = sql.executeQuery();
                                 while(res.next()){
                                     String crm = res.getString("crm");
                                     String cpf = res.getString("cpf");
                                     String horario = res.getString("horario");
                                     String data = res.getString("dataconsulta");
                                     String id = res.getString("id");
                                     
                                     try{
                                        Connection conn2 = null;
                                        ConectPostgree bd2 = new ConectPostgree();
                                        conn = bd2.conecta();
                                        String query = "select nome from paciente where cpf = "+"'"+cpf+"'";
                                        
                                        PreparedStatement sql2 = conn.prepareStatement(query);
                                        ResultSet res2 = sql2.executeQuery();
                                        while(res2.next()){
                                            
                                            nome_paciente = res2.getString("nome");
                                        }
                                    }catch (SQLException e){
                                         e.printStackTrace();
                                         out.println("N�o foi poss�vel conectar com o banco!");
                                     }
                                     try{
                                        Connection conn3 = null;
                                        conectbanco bdmysql = new conectbanco();
                                        conn3 = bdmysql.conecta();
                                        String query = "select nome from medico where crm = "+"'"+crm+"'";
                                       
                                        PreparedStatement sql2 = conn3.prepareStatement(query);
                                        ResultSet res2 = sql2.executeQuery();
                                        while(res2.next()){
                                            
                                            nome_medico = res2.getString("nome");
                                            
                                        }
                                    }catch (SQLException e){
                                         e.printStackTrace();
                                         out.println("N�o foi poss�vel conectar com o banco!");
                                     } 
                                     if(!nome_medico.equalsIgnoreCase("") && !nome_paciente.equalsIgnoreCase("")){
                                 
                         %>
                            <option value="<%=id%> - <%=horario%> - <%=data%> - <%=nome_paciente%> - <%=nome_medico%>"><%=id%> - <%=horario%> - <%=data%> - <%=nome_paciente%> - <%=nome_medico%></option>
                                <%}%>
                        <%    }
                             }catch (SQLException e){
                                  e.printStackTrace();
                                  out.println("N�o foi poss�vel conectar com o banco!");
                              }
                        %>
                </select>
                <div class="field"> <input style="width: 450px;" type="date" name="dataConsulta" value="${consulta.dataConsulta}"/></div>
                <div class="field"><input style="width: 450px;" type="time" name="horario" placeholder="Horario"  value="${consulta.horario}"/></div>
                <div class="field"><input style="width: 450px;" name="valor"  placeholder="Valor" value="${consulta.valor}" /></div>
                <div class="field"><input style="width: 450px;" name="observacao"  placeholder="Observa��o" value="${consulta.observacao}" /></div>
                
                
                
                <div class = "invis">
                    <select name="acao" required>
                        <option  value="Incluir">Incluir</option>
                        <option  selected value="Editar">Editar</option>
                        <option value="Excluir">Excluir</option>
                        <option value="Consultar">Consultar</option>
                    </select>
                </div>
                
                
                <div id="btn-login"><input style="width: 450px;"  type="submit" value="Editar" /></div>
                <input style="width: 450px;" type="reset" value="Limpar"> <br />
            </form>
            
			
        </div>
    </div>

</body>
</html>