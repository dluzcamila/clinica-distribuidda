<%-- 
    Document   : login
    Created on : 09/11/2016, 22:11:24
    Author     : Camila da Luz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
     
    <div id="login-titulo">
	<h1 style = "color:#3CB371; text-align:center; padding-bottom:10px;">Clínica Distribuída</h1>
    </div>
    <div id="login-container">
	<div id="login-form">
            <form action="LoginServlet" method="post">
		<div class="field"><input type="text" placeholder="Nickname" name="nickname" required="required" /></div>
		<div class="field"><input type="password" placeholder="Senha" name="userpass" required="required" /></div>
		<div id="btn-login"><input  type="submit" value="Login" /></div>
            </form>
        </div>
	<a href="cadastro_usuario.jsp" style = "text-align:center; margin:0px auto; display:block; color:#008B45"><blond>Cadastrar Usuário</a></blond>
	<h6 style="display: block; text-align: center;">Clínica Distribuída &copy; 2016</h6>
    </div>

</body>
</html>
