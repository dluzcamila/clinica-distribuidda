<!DOCTYPE html>
<%@page import="teste.conectbanco"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.swing.JComboBox"%>
<html>
<head>
        <title>M�dicos - Excluir</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="script.js" type="text/javascript"></script>
</head>
<body onload="ready()" onresize="adjust()">
	<nav id="menu">
        <ul>
            <li><a href="consulta.jsp">Consultas</a></li>
            <li><a href="medico.jsp">M�dicos</a></li>
            <li><a href="paciente.jsp">Pacientes</a></li>
        </ul>
    </nav>
    <div id="topbar">
        <h1>Cl�nica Distrubu�da</h1>
        <div id="user_nav">
            <a href="index.html">Sair</a>
        </div>
    </div>
	<div id="sidebar">
            <a href="editMedico.jsp" style="margin-top:34px"><span class="proj-icon"></span>Editar</a>
            <a href="excluiMedico.jsp" style="margin-top:34px"><span class="proj-icon"></span>Excluir</a>
            <a href="consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>P�gina Inicial</a>
        </div>

	<div id="login-form">
        <div id="login-container2">
            <p style="display: block; width: 500px; margin-left: 150px; padding-bottom: 10px; color:#3CB371; font-size: 20px;">Excluir M�dico</p>
            <form method="post" action="MedicoServlet" id="formCadastro" name="formulario" onsubmit="return validarSenha(this);">
                <select style="width: 460px; height: 33px; margin-bottom: 3px;" name="nome" class="campos" style="width: 381px;" onkeypress="cbboxSearch(this, event); return false;">
                    <option value="0">Selecione o M�dico</option>
                    <%
                            JComboBox nat_;
                            nat_ = new JComboBox();
                            try{
                                 Connection conn = null;
                                 conectbanco bd = new conectbanco();
                                 conn = bd.conecta();
                                 PreparedStatement sql = conn.prepareStatement("SELECT nome, crm FROM medico");
                                 ResultSet res = sql.executeQuery();
                                 while(res.next()){
                                     String x = res.getString("nome");
                                     String d = res.getString("crm");

                         %>
                         <option value="<%=x%>-<%=d%>"><%=x%>-<%=d%></option>

                        <%    }
                             }catch (SQLException e){
                                  e.printStackTrace();
                                  out.println("N�o foi poss�vel conectar com o banco!");
                              }
                        %>
                </select>
                
                
                
                <div class = "invis">
                    <select name="acao" required>
                        <option  value="Incluir">Incluir</option>
                        <option  value="Editar">Editar</option>
                        <option selected value="Excluir">Excluir</option>
                        <option value="Consultar">Consultar</option>
                    </select>
                </div>
                
                
                <div id="btn-login"><input style="width: 450px;"  type="submit" value="Excluir" /></div>
                <input style="width: 450px;" type="reset" value="Limpar"> <br />
            </form>
            
			
        </div>
    </div>

</body>
</html>