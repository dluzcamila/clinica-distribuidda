/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Camila da Luz
 */
public class ConectPostgree {
    protected Connection conn;
    protected String mensagem;
    public Connection conecta() {
    // carraga driver jdbc
    try {
      Class.forName("org.postgresql.Driver").newInstance();
    }
    catch (Exception ex) {
      mensagem = "Driver não carregado!";
      System.out.println("erro:"+mensagem);
    }

    // faz a conexão
    try {
    conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/teste","postgres","12345");
     return conn;   
    }
    catch (SQLException ex) {
      mensagem = ex.getMessage();
      System.out.println("erro:"+mensagem);
    }
        return null;
  }
    
}
