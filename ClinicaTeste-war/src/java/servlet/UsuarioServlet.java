/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import ejb.UsuarioEJB;
import entidade.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Camila da Luz
 */
@WebServlet(name = "UsuarioServlet", urlPatterns = {"/UsuarioServlet"})
public class UsuarioServlet extends HttpServlet { 

    @EJB
    private UsuarioEJB usuarioEJB;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String acao = request.getParameter("acao");
		String destino = "login.jsp";
		String mensagem = "";
		List<Usuario> lista = new ArrayList<>();
		Usuario usuario = new Usuario();
                
                try {
                    //Se a ação for DIFERENTE de Listar são lidos os dados da tela
                    if (!acao.equalsIgnoreCase("Listar")) {
			usuario.setNome(request.getParameter("nome"));
			usuario.setNickname(request.getParameter("nickname"));
			usuario.setFuncao(request.getParameter("funcao"));
			usuario.setSenha(request.getParameter("senha"));
                    }
 
                    if (acao.equalsIgnoreCase("Incluir")) {
                        System.out.print(usuario.toString());
                    // Verifica se a matrícula informada já existe no Banco de Dados
                    // Se existir enviar uma mensagem senão faz a inclusão
			if (usuarioEJB.existe(usuario)) {
                            System.out.print("NickName informado já existe!");
                            mensagem = "NickName informado já existe!";
                            
			} else {
                            usuarioEJB.inserir(usuario);
				}
			} else if (acao.equalsIgnoreCase("Alterar")) {
				usuarioEJB.alterar(usuario);
			} else if (acao.equalsIgnoreCase("Excluir")) {
				usuarioEJB.excluir(usuario);
			} else if (acao.equalsIgnoreCase("Consultar")) {
				request.setAttribute("usuario", usuario);
				usuario = usuarioEJB.consultar(usuario);
				destino = "cadastroUsuario.jsp";
			}
		} catch (Exception e) {
			mensagem += e.getMessage();
			destino = "erro.jsp";
			e.printStackTrace();
		}
 
		// Se a mensagem estivsucer vazia significa que houve sucesso!
		// Senão será exibida a tela de erro do sistema.
		if (mensagem.length() == 0) {
			mensagem = "Aluno Cadastrado com sucesso!";
		} else {
                    
			destino = "erro.jsp";
		}
 
		
		RequestDispatcher rd = request.getRequestDispatcher(destino);
		rd.forward(request, response);
 
 
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AlunoServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
