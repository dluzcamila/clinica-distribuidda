/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import ejb.ConsultaEJB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import entidade.Consulta;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import static java.time.Clock.system;
import javax.servlet.RequestDispatcher;
import teste.ConectPostgree;


/**
 *
 * @author Camila da Luz
 */
@WebServlet(name = "ConsultaServlet", urlPatterns = {"/ConsultaServlet"})
public class ConsultaServlet extends HttpServlet {

    @EJB
    private ConsultaEJB consultaEJB;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        String acao = request.getParameter("acao");
		String destino = "consulta.jsp";
		String mensagem = "";
                List<Consulta> lista = new ArrayList<>();
		Consulta consulta = new Consulta();
                Integer id2 = 0;
                String select_medico;
                String select_consulta;
                String select_paciente;
                String dados_medico[];
                String dados_paciente[];
                String dados_consulta[];
                
                
                if (acao.equalsIgnoreCase("Incluir")) {
                    select_medico = request.getParameter("medico");
                    dados_medico = select_medico.split("-");
           
                    consulta.setCrm(dados_medico[1]);
                    
                    
                    select_paciente = request.getParameter("paciente");
                    dados_paciente = select_paciente.split("-");
           
                    consulta.setCpf(dados_paciente[1]);
                }
                if (acao.equalsIgnoreCase("Excluir")) {
                    select_consulta = request.getParameter("consulta");
                    System.out.print(select_consulta);
                    dados_consulta = select_consulta.split("-");
                    System.out.print(dados_consulta[0].trim());
                    System.out.print("'"+dados_consulta[0].trim()+"'");
                    
                    consulta.setId(Integer.parseInt(dados_consulta[0].trim()));
                }
                if (acao.equalsIgnoreCase("Editar")) {
                    select_consulta = request.getParameter("consulta");
                    dados_consulta = select_consulta.split("-");
                    int id = Integer.parseInt(dados_consulta[0].trim());
                    consulta.setId(Integer.parseInt(dados_consulta[0].trim()));
                    try{
                        Connection conn = null;
                        ConectPostgree bd2 = new ConectPostgree();
                        String teste = "SELECT crm, cpf  FROM consulta where id =" + id;
                        conn = bd2.conecta();
                        PreparedStatement sql2 = conn.prepareStatement(teste);
               
                
                        ResultSet res = sql2.executeQuery();
                        while(res.next()){
                            consulta.setCrm(res.getString("crm"));
                            consulta.setCpf(res.getString("cpf"));
                            
                    
                   
                    
                        }
                        }catch (SQLException e){
                            e.printStackTrace();
                            out.println("Não foi possível conectar com o banco!");
                        }
                   
                    try{
                        Connection conn = null;
                        ConectPostgree bd2 = new ConectPostgree();
                        String teste = "SELECT dataconsulta, horario, valor, observacao FROM consulta where id =" + id;
                        System.out.print(teste);
                        conn = bd2.conecta();
                        PreparedStatement sql2 = conn.prepareStatement(teste);
               
                
                        ResultSet res = sql2.executeQuery();
                        while(res.next()){
                            if (request.getParameter("dataConsulta").equals("")){
                                consulta.setDataConsulta(res.getString("dataconsulta"));
                            
                            }
                            if (request.getParameter("horario").equals("")){
                                consulta.setHorario(res.getString("horario"));
                            
                            }
                            if (request.getParameter("valor").equals("")){
                                consulta.setValor(res.getString("valor"));
                            
                            }
                            if (request.getParameter("observacao").equals("")){
                               consulta.setObservacao(res.getString("observacao"));
                            
                            }
                            
                            
                            
                    
                   
                    
                        }
                        }catch (SQLException e){
                            e.printStackTrace();
                            out.println("Não foi possível conectar com o banco!");
                        }
                    
                 

                    
                }
                
 
		try {
                    if (!acao.equalsIgnoreCase("Listar")) {
                        if (acao.equalsIgnoreCase("Incluir")){
                           
                            consulta.setValor(request.getParameter("valor"));
                            consulta.setHorario(request.getParameter("horario"));
                            consulta.setDataConsulta(request.getParameter("dataConsulta"));
                            consulta.setObservacao(request.getParameter("observacao"));
                            try{
                        Connection conn = null;
                        ConectPostgree bd = new ConectPostgree();
                        conn = bd.conecta();
                        PreparedStatement sql = conn.prepareStatement("SELECT MAX(id) FROM consulta");
                        ResultSet res = sql.executeQuery();
                  
                        while(res.next()){
                           
                            String teste = res.getString("max");
                           
                            if(teste==null){}
                            else{
                                id2 = Integer.parseInt(res.getString("max"));
                                
                            }
                           

                         
                        }
                        }catch (SQLException e){
                            e.printStackTrace();
                            out.println("Não foi possível conectar com o banco!");
                        }
                    consulta.setId(id2+1);
                        
                        
                        
                        }
                        
                        else if(acao.equalsIgnoreCase("Editar")){
                             
                            if (!request.getParameter("dataConsulta").equals("")){
                                consulta.setDataConsulta(request.getParameter("dataConsulta"));
                            
                            }
                            if (!request.getParameter("horario").equals("")){
                                consulta.setHorario(request.getParameter("horario"));
                            
                            }
                            if (!request.getParameter("valor").equals("")){
                                consulta.setValor(request.getParameter("valor"));
                            
                            }
                            if (!request.getParameter("observacao").equals("")){
                              System.out.print("observacao");
                               consulta.setObservacao(request.getParameter("observacao"));
                            
                            }
                        
                        
                        }
                    
                    
                    
                }
 
                    if (acao.equalsIgnoreCase("Incluir")) {
			if (consultaEJB.existe(consulta)) {
                            mensagem = "Matrícula informada já existe!";
			} else {
                            consultaEJB.inserir(consulta);
			}
                    } else if (acao.equalsIgnoreCase("Editar")) {
                         System.out.print(consulta.toString());
                            consultaEJB.alterar(consulta);
                    } else if (acao.equalsIgnoreCase("Excluir")) {
			consultaEJB.excluir(consulta);
                    } else if (acao.equalsIgnoreCase("Consultar")) {
			request.setAttribute("aluno", consulta);
			consulta = consultaEJB.consultar(consulta);
			destino = "aluno.jsp";
                    }
		} catch (Exception e) {
			mensagem += e.getMessage();
			destino = "erro.jsp";
			e.printStackTrace();
		}
 
		// Se a mensagem estiver vazia significa que houve sucesso!
		// Senão será exibida a tela de erro do sistema.
		if (mensagem.length() == 0) {
			mensagem = "Aluno Cadastrado com sucesso!";
		} else {
			destino = "erro.jsp";
		}
 
		RequestDispatcher rd = request.getRequestDispatcher(destino);
		rd.forward(request, response);
 
 
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AlunoServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    

}
