/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import ejb.MedicoEJB;
import entidade.Medico;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import teste.ConectPostgree;
import teste.conectbanco;

/**
 *
 * @author Camila da Luz
 */
@WebServlet(name = "MedicoServlet", urlPatterns = {"/MedicoServlet"})
public class MedicoServlet extends HttpServlet {

    @EJB
    private MedicoEJB medicoEJB;

     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        String acao = request.getParameter("acao");
	String destino = "medico.jsp";
	String mensagem = "";
        String select;
        String nome_crm[];
        String email_banco="";
        String data_banco = "";
        String especialidade_banco= "";
        String rg_banco="";
        String telefone_banco="";
        String cpf_banco="";
        
	List<Medico> lista = new ArrayList<>();
	Medico medico = new Medico();
        
        if (acao.equalsIgnoreCase("Editar")) {
            select = request.getParameter("nome");
            nome_crm = select.split("-");
           
            medico.setNome(nome_crm[0]);
            medico.setCrm(nome_crm[1]);
            try{
                Connection conn = null;
                conectbanco bd2 = new conectbanco();
                String teste = "SELECT email, especialidade, rg, telefone, datanascimento, cpf FROM medico where crm =" + "'" +nome_crm[1]+"'";
                conn = bd2.conecta();
                PreparedStatement sql2 = conn.prepareStatement(teste);
               
                
                ResultSet res = sql2.executeQuery();
                while(res.next()){
                    email_banco = res.getString("email");
                    data_banco = res.getString("datanascimento");
                    especialidade_banco = res.getString("especialidade");
                    rg_banco = res.getString("rg");
                    telefone_banco = res.getString("telefone");
                    cpf_banco = res.getString("cpf");
                    
                    
                }
            }catch (SQLException e){
                e.printStackTrace();
                out.println("Não foi possível conectar com o banco!");
            }
            if (request.getParameter("dataNascimento").equals("")){
                medico.setDataNascimento(data_banco);
            
            }
            if (request.getParameter("email").equals("")){
                medico.setEmail(email_banco);
            
            }
            if (request.getParameter("telefone").equals("")){
               
                medico.setTelefone(telefone_banco);
            
            }
            if (request.getParameter("rg").equals("")){
                
                medico.setRg(rg_banco);
            
            }
            if (request.getParameter("especialidade").equals("")){
                
                medico.setEspecialidade(especialidade_banco);
            
            }
            if (request.getParameter("cpf").equals("")){
                
                medico.setCpf(cpf_banco);
            
            }
            
            
            
            
        }
        else if (acao.equalsIgnoreCase("Excluir")) {
            System.out.print("exclui1");
            select= request.getParameter("nome");
            nome_crm = select.split("-");
            
            
           
            medico.setNome(nome_crm[0]);
            medico.setCrm(nome_crm[1]);
            
            
            
            
        }
        
        
        else{
            medico.setNome(request.getParameter("nome"));
            medico.setCrm(request.getParameter("crm"));
        }
        try {
            //Se a ação for DIFERENTE de Listar são lidos os dados da tela
            if (!acao.equalsIgnoreCase("Listar") ) {
                if (!acao.equalsIgnoreCase("Excluir") ) {
                    if (!request.getParameter("dataNascimento").equals("")){
                         medico.setDataNascimento(request.getParameter("dataNascimento"));


                    }
                    if (!request.getParameter("email").equals("")){
                        medico.setEmail(request.getParameter("email"));


                    }
                    if (!request.getParameter("telefone").equals("")){
                        medico.setTelefone(request.getParameter("telefone"));


                    }
                    if (!request.getParameter("rg").equals("")){
                         medico.setRg(request.getParameter("rg"));

                    }
                    if (!request.getParameter("especialidade").equals("")){
                         medico.setEspecialidade(request.getParameter("especialidade"));


                    }
                    if (!request.getParameter("cpf").equals("")){
                         medico.setCpf(request.getParameter("cpf"));


                    }
                }

               
		
               
               
               
 
		
                    
            }
             
            if (acao.equalsIgnoreCase("Incluir")) {
				// Verifica se a matrícula informada já existe no Banco de Dados
				// Se existir enviar uma mensagem senão faz a inclusão
		if (medicoEJB.existe(medico)) {
                    mensagem = "CRM informada já existe!";
		} else {
                    medicoEJB.inserir(medico);
                }
                } else if (acao.equalsIgnoreCase("Excluir")) {
                    System.out.print(medico.toString());
                    medicoEJB.excluir(medico);
                
		} else if (acao.equalsIgnoreCase("Editar")) {
                    medicoEJB.alterar(medico);
		
		} else if (acao.equalsIgnoreCase("Consultar")) {
                    request.setAttribute("aluno", medico);
                    medico = medicoEJB.consultar(medico);
                    destino = "medico.jsp";
		}
		} catch (Exception e) {
			mensagem += e.getMessage();
			destino = "erro.jsp";
			e.printStackTrace();
		}
 
		// Se a mensagem estiver vazia significa que houve sucesso!
		// Senão será exibida a tela de erro do sistema.
		if (mensagem.length() == 0) {
			mensagem = "Medico Cadastrado com sucesso!";
		} else {
			destino = "erro.jsp";
		}
 
		
		RequestDispatcher rd = request.getRequestDispatcher(destino);
		rd.forward(request, response);
 
 
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AlunoServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
 
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
