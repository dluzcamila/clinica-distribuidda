/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import ejb.PacienteEJB;
import entidade.Paciente;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import teste.ConectPostgree;

/**
 *
 * @author Camila da Luz
 */
@WebServlet(name = "PacienteServlet", urlPatterns = {"/PacienteServlet"})
public class PacienteServlet extends HttpServlet {

    @EJB
    private PacienteEJB pacienteEJB;

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
 
        String acao = request.getParameter("acao");
	String destino = "paciente.jsp";
	String mensagem = "";
        String select;
        String nome_cpf[];
        String email_banco="";
        String data_banco = "";
        String obs_banco= "";
        String rg_banco="";
        String telefone_banco="";
        String rua_banco="";
        String bairro_banco="";
        String uf_banco="";
        String cep_banco="";
        String cidade_banco="";
            
	List<Paciente> lista = new ArrayList<>();
	Paciente paciente = new Paciente();
        
        
        
        
        if (acao.equalsIgnoreCase("Editar")) {
            select = request.getParameter("nome");
            nome_cpf = select.split("-");
            paciente.setNome(nome_cpf[0]);
            paciente.setCpf(nome_cpf[1]);
            try{
                Connection conn = null;
                ConectPostgree bd2 = new ConectPostgree();
                String teste = "SELECT rua, cep, cidade, uf, bairro, email, observacao, rg, telefone, datanascimento FROM paciente where cpf =" + "'" +nome_cpf[1]+"'";
                System.out.print(teste);
                conn = bd2.conecta();
                PreparedStatement sql2 = conn.prepareStatement(teste);
               
                
                ResultSet res = sql2.executeQuery();
                while(res.next()){
                    
                    email_banco = res.getString("email");
                    data_banco = res.getString("datanascimento");
                    obs_banco = res.getString("observacao");
                    rg_banco = res.getString("rg");
                    telefone_banco = res.getString("telefone");
                    uf_banco = res.getString("uf");
                    bairro_banco = res.getString("bairro");
                    cidade_banco = res.getString("cidade");
                    cep_banco = res.getString("cep");
                    rua_banco = res.getString("rua");
                    
                }
            }catch (SQLException e){
                e.printStackTrace();
                out.println("Não foi possível conectar com o banco!");
            }
                                 
        
            
                
            if (request.getParameter("dataNascimento").equals("")){
                System.out.print("data null");
                paciente.setDataNascimento(data_banco);
            
            }
            if (request.getParameter("email").equals("")){
                 System.out.print("email null");
                paciente.setEmail(email_banco);
            
            }
            if (request.getParameter("telefone").equals("")){
                System.out.print("telefone null");
                paciente.setTelefone(telefone_banco);
            
            }
            if (request.getParameter("rg").equals("")){
                System.out.print("rg null");
                paciente.setRg(rg_banco);
            
            }
            if (request.getParameter("observacao").equals("")){
                System.out.print("observacao null");
                paciente.setObservacao(obs_banco);
            
            }
               
            paciente.setRua(rua_banco);
            paciente.setBairro(bairro_banco);
            paciente.setUf(uf_banco);
            paciente.setCep(cep_banco);
            paciente.setCidade(cidade_banco);
            
            
            
            
          
            
            
            
            
        }
        else if (acao.equalsIgnoreCase("Excluir")) {
           select = request.getParameter("nome");
            nome_cpf = select.split("-");
           
            paciente.setNome(nome_cpf[0]);
            paciente.setCpf(nome_cpf[1]);
            
        }
        
        
        else{
            paciente.setNome(request.getParameter("nome"));
            paciente.setCpf(request.getParameter("cpf"));
            paciente.setCep(request.getParameter("cep"));
            paciente.setBairro(request.getParameter("bairro"));
            paciente.setRua(request.getParameter("rua"));
            paciente.setUf(request.getParameter("uf"));
            paciente.setCidade(request.getParameter("cidade"));
            
        }
       
        try {
            //Se a ação for DIFERENTE de Listar são lidos os dados da tela
            if (!acao.equalsIgnoreCase("Listar")) {
                if (!acao.equalsIgnoreCase("Excluir")){
                    if (!request.getParameter("dataNascimento").equals("")){
                    paciente.setDataNascimento(request.getParameter("dataNascimento"));

                    }
                    if (!request.getParameter("email").equals("")){
                        paciente.setEmail(request.getParameter("email"));

                    }
                    if (!request.getParameter("telefone").equals("")){
                        paciente.setTelefone(request.getParameter("telefone"));

                    }
                    if (!request.getParameter("rg").equals("")){
                        paciente.setRg(request.getParameter("rg"));

                    }
                    if (!request.getParameter("observacao").equals("")){
                        paciente.setObservacao(request.getParameter("observacao"));

                    }
                }


		
                
		
                
                
 
		
                    
            }
            System.out.print(paciente.toString());
            if (acao.equalsIgnoreCase("Incluir")) {
				// Verifica se a matrícula informada já existe no Banco de Dados
				// Se existir enviar uma mensagem senão faz a inclusão
		if (pacienteEJB.existe(paciente)) {
                    mensagem = "CRM informada já existe!";
		} else {
                    pacienteEJB.inserir(paciente);
                }
                } else if (acao.equalsIgnoreCase("Excluir")) {
                     
                    pacienteEJB.excluir(paciente);
                
		} else if (acao.equalsIgnoreCase("Editar")) {
                    System.out.println(paciente.toString());
                    pacienteEJB.alterar(paciente);
		
		} else if (acao.equalsIgnoreCase("Consultar")) {
                    request.setAttribute("aluno", paciente);
                    paciente = pacienteEJB.consultar(paciente);
                    destino = "paciente.jsp";
		}
		} catch (Exception e) {
			mensagem += e.getMessage();
			destino = "erro.jsp";
			e.printStackTrace();
		}
 
		// Se a mensagem estiver vazia significa que houve sucesso!
		// Senão será exibida a tela de erro do sistema.
		if (mensagem.length() == 0) {
			mensagem = "Medico Cadastrado com sucesso!";
		} else {
			destino = "erro.jsp";
		}
 
		
		RequestDispatcher rd = request.getRequestDispatcher(destino);
		rd.forward(request, response);
 
 
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AlunoServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PacienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PacienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
