<%-- 
    Document   : paciente
    Created on : 11/11/2016, 23:00:41
    Author     : Camila da Luz
--%>

<%@page import="teste.ConectPostgree"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="teste.conectbanco"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.swing.JComboBox"%>
<html>
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Pacientes</title>
            <link rel="stylesheet" type="text/css" href="style.css">
            <script src="script.js"> type="text/javascript"></script>
    </head>
    <body onload="ready()" onresize="adjust()">
        <nav id="menu">
            <ul>
                <li><a href="consulta.jsp">Consultas</a></li>
                <li><a href="medico.jsp">Médicos</a></li>
                <li><a href="paciente.jsp">Pacientes</a></li>
            </ul>
        </nav>
        <div id="topbar">
            <h1>Clínica Distrubuída</h1>
            <div id="user_nav">
                <a href="index.html">Sair</a>
            </div>
        </div>

        <div id="sidebar">
            <a href="edit_paciente.jsp" style="margin-top:34px"><span class="proj-icon"></span>Editar</a>
            <a href="exclui_paciente.jsp" style="margin-top:34px"><span class="proj-icon"></span>Excluir</a>
            <a href="welcome.jsp" style="margin-top:34px"><span class="proj-icon"></span>Página Inicial</a>
        </div>

        <div id="content">
            <h1 class="titulo-pgn">Pacientes Cadastrados: <br><br><a href="cadastro_paciente.jsp">Cadastrar Paciente</a></h1> 
            <ul id="proj-list">
            <%
                JComboBox nat_;
                nat_ = new JComboBox();
                try{
                    Connection conn = null;
                    ConectPostgree bd = new ConectPostgree();
                    conn = bd.conecta();
                    PreparedStatement sql = conn.prepareStatement("SELECT bairro, rua, cidade, uf, nome, cpf, datanascimento, email, observacao, rg, telefone FROM paciente");
                    ResultSet res = sql.executeQuery();
                    while(res.next()){
                        String nome = res.getString("nome");
                        String cpf = res.getString("cpf");
                        String telefone = res.getString("telefone");
                        String email = res.getString("email");
                        String datanascimento = res.getString("datanascimento");
                        String rg = res.getString("rg");
                        String observacao = res.getString("observacao");
                        String bairro = res.getString("bairro");
                        String rua = res.getString("rua");
                        String uf = res.getString("uf");
                        String cidade = res.getString("cidade");
                        
                        
                    %>
                    <li class="border_gray"> 
                        <h1><span>Paciente: </span> <%= nome %> </h1><br>
                        <p><span>Data Nascimento:</span> <%= datanascimento %> </p><br>
                        <p><span>CPF: </span> <%= cpf %> </p>
                        <p><span>RG: </span> <%= rg %> </p><br>
                        <p><span>Email: </span> <%= email %></p>
                        <p><span>Telefone: </span> <%= telefone %></p><br>
                        <p><span>Observacao: </span> <%= observacao %></p><br>
                        <p><span>Cidade: </span> <%= cidade %></p>
                        <p><span>Estado: </span> <%= uf %></p>
                        <p><span>Rua </span> <%= rua %></p>
                        <p><span>Bairro: </span> <%= bairro %></p>
                    </li> 
                <%
                    }
                }catch (SQLException e){
                    e.printStackTrace();
                    out.println("Não foi possível conectar com o banco!");
                }
                %>
            </ul>
            
        </div>

    </body>
</html>	