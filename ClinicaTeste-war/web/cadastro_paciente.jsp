<!DOCTYPE html>
<html>
    <head>
        <title>Paciente - Cadastro</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="script.js" type="text/javascript"></script>
    </head>
    <body onload="ready()" onresize="adjust()">
        <nav id="menu">
            <ul>
                <li><a href="consulta.jsp">Consultas</a></li>
                <li><a href="medico.jsp">M�dicos</a></li>
                <li><a href="paciente.jsp">Pacientes</a></li>
            </ul>
        </nav>
        <div id="topbar">
            <h1>Cl�nica Distrubu�da</h1>
            <div id="user_nav">
                <a href="index.html">Sair</a>
            </div>
        </div>
        <div id="sidebar">
            <a href="edit_paciente.jsp" style="margin-top:34px"><span class="proj-icon"></span>Editar</a>
            <a href="exclui_paciente.jsp" style="margin-top:34px"><span class="proj-icon"></span>Excluir</a>
            <a href="consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>P�gina Inicial</a>
        </div>

        <div id="login-form">
            <div id="login-container2">
                <p style="display: block; width: 500px; margin-left: 150px; padding-bottom: 10px; color:#3CB371; font-size: 20px;">Cadastrar Paciente</p>

                <form method="post" action="PacienteServlet" id="formCadastro" name="formulario" onsubmit="return validarSenha(this);">
                    <div class="field"><input style="width: 450px;" name="nome" required placeholder="Nome" value="${paciente.nome}" /></div>
                    <div class="field"> <input style="width: 450px;" type="date" name="dataNascimento" value="${paciente.dataNascimento}"/></div>
                    <div class="field"><input style="width: 450px;" type="email" name="email" placeholder="Email"  value="${paciente.email}"/></div>
                    <div class="field"><input style="width: 450px;" type="tel" name="telefone" placeholder="9999-9999" value="${paciente.telefone}" /></div>
                    <div class="field"><input style="width: 450px;" name="cpf" required placeholder="CPF" value="${paciente.cpf}" /></div>
                    <div class="field"><input style="width: 450px;" name="rg" required placeholder="RG" value="${paciente.rg}" /></div>
                    <div class="field"><input style="width: 450px;" name="observacao" required placeholder="Observa��o" value="${paciente.observacao}" /></div>
                    <p style="display: block; width: 500px; margin-left: 170px; padding-bottom: 10px; color:#3CB371; font-size: 20px;">Endere�o</p>
                    <div class="field" style=" margin-left: 80px;" ><input required placeholder="CEP" name="cep" type="text" id="cep" value="" size="10" maxlength="9"onblur="pesquisacep(this.value);" /></div>
                    <div class="field" style=" margin-left: 80px;"><input required placeholder="Rua" name="rua" type="text" id="rua" size="60" /></div>
                    <div class="field" style=" margin-left: 80px;"><input required placeholder="Bairro" name="bairro" type="text" id="bairro" size="40" /></div>
                    <div class="field" style=" margin-left: 80px;"><input required placeholder="Cidade" name="cidade" type="text" id="cidade" size="40" /></div>
                    <div class="field"style=" margin-left: 80px;"><input required placeholder="Estado" name="uf" type="text" id="uf" size="2" /></div>

                    <div class = "invis">

                        <input name="ibge" type="text" id="ibge" size="8" /></label><br />
                        <select name="acao" required>
                            <option selected value="Incluir">Incluir</option>
                            <option value="Alterar">Alterar</option>
                            <option value="Excluir">Excluir</option>
                            <option value="Consultar">Consultar</option>
                        </select>
                    </div>
                    <div id="btn-login"><input style="width: 450px;"  type="submit" value="Cadastrar" /></div>
                    <input style="width: 450px;" type="reset" value="Limpar"> <br />
                </form>


            </div>
        </div>

    </body>
</html>

<script type="text/javascript" >

function limpa_formul�rio_cep() {
        //Limpa valores do formul�rio de cep.
        document.getElementById('rua').value = ("");
        document.getElementById('bairro').value = ("");
        document.getElementById('cidade').value = ("");
        document.getElementById('uf').value = ("");
        document.getElementById('ibge').value = ("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value = (conteudo.logradouro);
            document.getElementById('bairro').value = (conteudo.bairro);
            document.getElementById('cidade').value = (conteudo.localidade);
            document.getElementById('uf').value = (conteudo.uf);
            document.getElementById('ibge').value = (conteudo.ibge);
        } //end if.
        else {
            //CEP n�o Encontrado.
            limpa_formul�rio_cep();
            alert("CEP n�o encontrado.");
        }
    }

    function pesquisacep(valor) {

        //Nova vari�vel "cep" somente com d�gitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Express�o regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value = "...";
                document.getElementById('bairro').value = "...";
                document.getElementById('cidade').value = "...";
                document.getElementById('uf').value = "...";
                document.getElementById('ibge').value = "...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = '//viacep.com.br/ws/' + cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conte�do.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep � inv�lido.
                limpa_formul�rio_cep();
                alert("Formato de CEP inv�lido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formul�rio.
            limpa_formul�rio_cep();
        }
    }
    ;

</script>