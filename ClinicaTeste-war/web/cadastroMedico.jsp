<!DOCTYPE html>
<html>
<head>
        <title>M�dico - Cadastro</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="script.js" type="text/javascript"></script>
</head>
<body onload="ready()" onresize="adjust()">
	<nav id="menu">
        <ul>
            <li><a href="consulta.jsp">Consultas</a></li>
            <li><a href="medico.jsp">M�dicos</a></li>
            <li><a href="paciente.jsp">Pacientes</a></li>
        </ul>
    </nav>
    <div id="topbar">
        <h1>Cl�nica Distrubu�da</h1>
        <div id="user_nav">
            <a href="index.html">Sair</a>
        </div>
    </div>
	<div id="sidebar">
            <a href="editMedico.jsp" style="margin-top:34px"><span class="proj-icon"></span>Editar</a>
            <a href="excluiMedico.jsp" style="margin-top:34px"><span class="proj-icon"></span>Excluir</a>
            <a href="consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>P�gina Inicial</a>
        </div>

	<div id="login-form">
        <div id="login-container2">
            <p style="display: block; width: 500px; margin-left: 150px; padding-bottom: 10px; color:#3CB371; font-size: 20px;">Cadastrar M�dico</p>
            <form method="post" action="MedicoServlet" id="formCadastro" name="formulario" onsubmit="return validarSenha(this);">
                <div class="field"><input style="width: 450px;" name="nome" required placeholder="Nome" value="${medico.nome}" /></div>
                <div class="field"> <input style="width: 450px;" type="date" required name="dataNascimento" value="${medico.dataNascimento}"/></div>
                <div class="field"><input style="width: 450px;" type="email" name="email" placeholder="Email"  value="${medico.email}"/></div>
                <div class="field"><input style="width: 450px;" name="crm" required placeholder="CRM" value="${medico.crm}" /></div>
                <div class="field"><input style="width: 450px;" name="cpf" required placeholder="CPF" value="${medico.cpf}" /></div>
                <div class="field"><input style="width: 450px;" name="rg" required placeholder="RG" value="${medico.rg}" /></div>
                <div class="field"><input style="width: 450px;" type="tel" required name="telefone" placeholder="9999-9999" value="${medico.telefone}" /></div>
               <select style="width: 465px;" name="especialidade" required>
                        <option selected value="teste">Selecione a Especialidade</option>
                        <option value="Ortopedia">Ortopedia</option>
                        <option value="Ginecologia">Ginecologia</option>
                        <option value="Cardiologia">Cardiologia</option>
                        <option value="Dermatologia">Dermatologia</option>
                        <option value="Fisioterapia">Fisioterapia</option>
                        <option value="Neurologia">Neurologia</option>
                </select>
                <br>
                <div class = "invis">
                    <select name="acao" required>
                        <option selected value="Incluir">Incluir</option>
                        <option value="Alterar">Alterar</option>
                        <option value="Excluir">Excluir</option>
                        <option value="Consultar">Consultar</option>
                    </select>
                </div>
                <div id="btn-login"><input style="width: 450px;"  type="submit" value="Cadastrar" /></div>
                <input style="width: 450px;" type="reset" value="Limpar"> <br />
            </form>
            
			
        </div>
    </div>

</body>
</html>