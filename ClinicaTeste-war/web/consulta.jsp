<%-- 
    Document   : welcome
    Created on : 09/11/2016, 22:55:17
    Author     : Camila da Luz
--%>

<%@page import="teste.conectbanco"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="teste.ConectPostgree"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.swing.JComboBox"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Consultas</title>
            <link rel="stylesheet" type="text/css" href="style.css">
            <script src="script.js"> type="text/javascript"></script>
    </head>
    <body onload="ready()" onresize="adjust()">
        
        <nav id="menu">
            <ul>
                <li><a href="consulta.jsp">Consultas</a></li>
                <li><a href="medico.jsp">Médicos</a></li>
                <li><a href="paciente.jsp">Pacientes</a></li>
            </ul>
        </nav>
        <div id="topbar">
            <h1>Clínica Distrubuída</h1>
            <div id="user_nav">
               
                <a href="index.html">Sair</a>
            </div>
        </div>

        <div id="sidebar">
            <a href="exclui_consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>Excluir</a>
            <a href="edit_consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>Editar</a>
            <a href="consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>Pagina Inicial</a>
        </div>
        <div id="content">
            <h1 class="titulo-pgn">Consultas Cadastradas: <br><br><a href="cadastro_consulta.jsp">Nova Consulta</a></h1> 
             <ul id="proj-list">
        
        <%
            JComboBox nat_;
            nat_ = new JComboBox();
            try{
                Connection conn = null;
                ConectPostgree bd = new ConectPostgree();
                conn = bd.conecta();
                String nome_paciente = "";
                String nome_medico= "";
                String especialidade_medico= "";
                PreparedStatement sql = conn.prepareStatement("SELECT id, crm, cpf, horario, dataconsulta FROM consulta");
                ResultSet res = sql.executeQuery();
                while(res.next()){
                    String crm = res.getString("crm");
                    String cpf = res.getString("cpf");
                    String horario = res.getString("horario");
                    String data = res.getString("dataconsulta");
                    String id = res.getString("id");
                                     
                    try{
                        Connection conn2 = null;
                        ConectPostgree bd2 = new ConectPostgree();
                        conn = bd2.conecta();
                        String query = "select nome from paciente where cpf = "+"'"+cpf+"'";
                                        
                        PreparedStatement sql2 = conn.prepareStatement(query);
                        ResultSet res2 = sql2.executeQuery();
                        while(res2.next()){
                            nome_paciente = res2.getString("nome");
                        }
                    }catch (SQLException e){
                        e.printStackTrace();
                        out.println("Não foi possível conectar com o banco!");
                    }
                    try{
                        Connection conn3 = null;
                        conectbanco bdmysql = new conectbanco();
                        conn3 = bdmysql.conecta();
                        String query = "select nome, especialidade from medico where crm = "+"'"+crm+"'";
                                       
                        PreparedStatement sql2 = conn3.prepareStatement(query);
                        ResultSet res2 = sql2.executeQuery();
                        while(res2.next()){
                            nome_medico = res2.getString("nome");
                            especialidade_medico = res2.getString("especialidade");
                        }
                    }catch (SQLException e){
                        e.printStackTrace();
                        out.println("Não foi possível conectar com o banco!");
                    }
                    
                        if(!nome_medico.equalsIgnoreCase("") && !nome_paciente.equalsIgnoreCase("")){
                            
                        
                                 
            %>
           
          
                <li class="border_gray"> 
                    <h1><span>Consulta: </span> <%=especialidade_medico%></h1><br>
                    <p><span>Médico:</span> <%=nome_medico%> </p><br>
                    <p><span>Paciente: </span><%=nome_paciente%></p><br>
                    <p><span>Data | Horario: </span> <%=data%> - <%=horario%></p>
                </li>
            

            <%}%>

            <%}
            }catch (SQLException e){
                e.printStackTrace();
                out.println("Não foi possível conectar com o banco!");
            }
            %>
            </ul>
        </div>

    </body>
</html>	       
