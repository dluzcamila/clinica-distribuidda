<%-- 
    Document   : medico
    Created on : 11/11/2016, 23:00:17
    Author     : Camila da Luz
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="teste.conectbanco"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.swing.JComboBox"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Médicos</title>
            <link rel="stylesheet" type="text/css" href="style.css">
            <script src="script.js"> type="text/javascript"></script>
    </head>
    <body onload="ready()" onresize="adjust()">
        <nav id="menu">
            <ul>
                <li><a href="consulta.jsp">Consultas</a></li>
                <li><a href="medico.jsp">Médicos</a></li>
                <li><a href="paciente.jsp">Pacientes</a></li>
            </ul>
        </nav>
        <div id="topbar">
            <h1>Clínica Distrubuída</h1>
            <div id="user_nav">
                <a href="index.html">Sair</a>
            </div>
        </div>

        <div id="sidebar">
            <a href="editMedico.jsp" style="margin-top:34px"><span class="proj-icon"></span>Editar</a>
            <a href="excluiMedico.jsp" style="margin-top:34px"><span class="proj-icon"></span>Excluir</a>
            <a href="consulta.jsp" style="margin-top:34px"><span class="proj-icon"></span>Página Inicial</a>
        </div>

        <div id="content">
            <h1 class="titulo-pgn">Médicos Cadastrados: <br><br><a href="cadastroMedico.jsp">Cadastrar Médico</a></h1> 
            <ul id="proj-list">
            <%
                JComboBox nat_;
                nat_ = new JComboBox();
                try{
                    Connection conn = null;
                    conectbanco bd_mysql = new conectbanco();
                    conn = bd_mysql .conecta();
                    PreparedStatement sql = conn.prepareStatement("SELECT nome, cpf, especialidade, crm, datanascimento, email, rg, telefone FROM medico");
                    ResultSet res = sql.executeQuery();
                    while(res.next()){
                        String nome = res.getString("nome");
                        String cpf = res.getString("cpf");
                        String telefone = res.getString("telefone");
                        String email = res.getString("email");
                        String crm= res.getString("crm");
                        String datanascimento = res.getString("datanascimento");
                        String rg = res.getString("rg");
                        String especialidade = res.getString("especialidade");
                        
                        
                    %>
                    <li class="border_gray"> 
                        <h1><span>Médico: </span> <%= nome %> </h1><br>
                        <p><span>CRM:</span> <%= crm %> </p>
                        <p><span>Especialidade:</span> <%= especialidade %> </p><br>
                        <p><span>Data Nascimento:</span> <%= datanascimento %> </p><br>
                        <p><span>CPF: </span> <%= cpf %> </p>
                        <p><span>RG: </span> <%= rg %> </p><br>
                        <p><span>Email: </span> <%= email %></p>
                        <p><span>Telefone: </span> <%= telefone %></p><br>
                    </li> 
                <%
                    }
                }catch (SQLException e){
                    e.printStackTrace();
                    out.println("Não foi possível conectar com o banco!");
                }
                %>
            </ul>
        </div>

    </body>
</html>	
